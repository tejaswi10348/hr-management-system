package com.hrmanagement.bean;

import java.io.Serializable;
/**
 * this is a pojo class contains the variables for storing vacancy details  
 * @author Group H
 */
public class VacancyPojo implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer vacancy;
	
	public Integer getVacancy() {
		return vacancy;
	}
	public void setVacancy(Integer vacancy) {
		this.vacancy = vacancy;
	}
	@Override
	public String toString() {
		return "" + vacancy + "";
	}
}
