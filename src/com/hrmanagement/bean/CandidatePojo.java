package com.hrmanagement.bean;

import java.io.Serializable;
/**
 * this is a pojo class contains the variables for storing candidate details  
 * @author Group H
 */
public class CandidatePojo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String cFullName;
	private String cPhNumber;
	private String cEmail;
	private String cXAggregate;
	private String cXIIAggregate;
	private String cHighestQualification;
	private String cHPercentage;
	private String cDomain;
	
	public String getcFullName() {
		return cFullName;
	}
	public void setcFullName(String cFullName) {
		this.cFullName = cFullName;
	}
	public String getcPhNumber() {
		return cPhNumber;
	}
	public void setcPhNumber(String cPhNumber) {
		this.cPhNumber = cPhNumber;
	}
	public String getcEmail() {
		return cEmail;
	}
	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}
	public String getcXAggregate() {
		return cXAggregate;
	}
	public void setcXAggregate(String cXAggregate) {
		this.cXAggregate = cXAggregate;
	}
	public String getcXIIAggregate() {
		return cXIIAggregate;
	}
	public void setcXIIAggregate(String cXIIAggregate) {
		this.cXIIAggregate = cXIIAggregate;
	}
	public String getcHighestQualification() {
		return cHighestQualification;
	}
	public void setcHighestQualification(String cHighestQualification) {
		this.cHighestQualification = cHighestQualification;
	}
	public String getcHPercentage() {
		return cHPercentage;
	}
	public void setcHPercentage(String cHPercentage) {
		this.cHPercentage = cHPercentage;
	}
	public String getcDomain() {
		return cDomain;
	}
	public void setcDomain(String cDomain) {
		this.cDomain = cDomain;
	}
	@Override
	public String toString() {
		return "Full Name-" + cFullName + " ,Phone Number-" + cPhNumber + ", EmailId-" + cEmail
				+ ", X Percentage-" + cXAggregate + ", XII Percentage" + cXIIAggregate + 
				", Highest Qualification-"+ cHighestQualification + ", Percentage=" + cHPercentage + 
				", Domain=" + cDomain + "]";
	}
	
	
	
	
	
	
	
}
