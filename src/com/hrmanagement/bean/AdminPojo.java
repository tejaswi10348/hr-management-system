package com.hrmanagement.bean;

import java.io.Serializable;
/**
 * this is a pojo class contains the variables for storing admin details  
 * @author Group H
 */
public class AdminPojo  implements Serializable{

	private static final long serialVersionUID = 1L;
	private String userName;
	private String password;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "userName- " + userName + " , password- " + password;
	}
	
	
	
	
}
