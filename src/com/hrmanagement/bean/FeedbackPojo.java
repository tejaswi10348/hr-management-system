package com.hrmanagement.bean;

import java.io.Serializable;
/**
 * this is a pojo class contains the variables for storing feedback details  
 * @author Group H
 */
public class FeedbackPojo implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	private String domain;
	private String result;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String status) {
		this.result = status;
	}
	@Override
	public String toString() {
		return "name=" + name + ", domain=" + domain + ", result=" + result + "";
	}
	
	
	
	
	
	
	
	
	
	
}
 






