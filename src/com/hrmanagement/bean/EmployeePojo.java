package com.hrmanagement.bean;

import java.io.Serializable;
/**
 * this is a pojo class contains the variables for storing employee details  
 * @author Group H
 */
public class EmployeePojo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String eId;
	private String eName;
	private String ePassword;
	private String eEmail;
	private String ePhNumber;
		
	public String geteId() {
		return eId;
	}
	public void seteId(String eId) {
		this.eId = eId;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	public String getePassword() {
		return ePassword;
	}
	public void setePassword(String ePassword) {
		this.ePassword = ePassword;
	}
	public String geteEmail() {
		return eEmail;
	}
	public void seteEmail(String eEmail) {
		this.eEmail = eEmail;
	}
	public String getePhNumber() {
		return ePhNumber;
	}
	public void setePhNumber(String ePhNumber) {
		this.ePhNumber = ePhNumber;
	}
	@Override
	public String toString() {
		return "employeeId=" + eId + ", Name=" + eName + ", Password=" + ePassword + ", Email=" + eEmail
				+ ", Phone Number=" + ePhNumber + "";
	}
	
	
	}
	

