package com.hrmanagement.bean;
import java.io.Serializable;
/**
 * this is a pojo class contains the variables for storing training details  
 * @author Group H
 */
public class TrainingPojo  implements Serializable {

	private static final long serialVersionUID = 1L;
	private String empId;
	private double ePercentage; 
	private String eDomain;
	private double eAttendance;
	private double eSalary;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public double getePercentage() {
		return ePercentage;
	}
	public void setePercentage(double ePercentage) {
		this.ePercentage = ePercentage;
	}
	public String geteDomain() {
		return eDomain;
	}
	public void seteDomain(String eDomain) {
		this.eDomain = eDomain;
	}
	public double geteAttendance() {
		return eAttendance;
	}
	public void seteAttendance(double eAttendance) {
		this.eAttendance = eAttendance;
	}
	public double geteSalary() {
		return eSalary;
	}
	public void seteSalary(double eSalary) {
		this.eSalary = eSalary;
	}
	@Override
	public String toString() {
		return "EmployeeId=" + empId + ", Percentage=" + ePercentage + "% , Domain=" + eDomain
				+ ", Attendance=" + eAttendance + "% , Salary=" + eSalary + " CTC per annum";
	}
	
	
}
