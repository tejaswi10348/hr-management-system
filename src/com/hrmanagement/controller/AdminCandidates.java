package com.hrmanagement.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.hrmanagement.bean.CandidatePojo;
/**
 * this class has the methods for serialization and deserialization of candidates data
 * @author Group H
 */
public class AdminCandidates {
	//This file is to store the candidates.
	public static final String CANDIDATE_FILE = "candidates.txt";
	//This is an object of type Array list to store the candidates.
	static ArrayList<CandidatePojo> cList = new ArrayList<CandidatePojo>();
	/**
	 *This method is to add candidate details 
	 * @param candidate object
	 */
	public static void addCandidate(CandidatePojo candidate) {
		//Fetching candidate details from file
		cList = displayCandidates();
		//Adding candidate object to list
		cList.add(candidate);
	    try {
	    	FileOutputStream fout = new FileOutputStream(CANDIDATE_FILE);
	        ObjectOutputStream out = new ObjectOutputStream(fout);
	        //Writing object to list
	        out.writeObject(cList);
	        out.close();
	        fout.close();
	    } catch (Exception e) {}
	}	
	/**
	 * this method displays the candidates details
	 * @return ccandidates list
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<CandidatePojo> displayCandidates() { 
		try {
			FileInputStream fin = new FileInputStream(CANDIDATE_FILE);
			ObjectInputStream in = new ObjectInputStream(fin);
			//Reading object from list
			cList = (ArrayList<CandidatePojo>)in.readObject();
			in.close();
			fin.close();
		} catch (Exception e) {}
	return cList;
	}	
}
