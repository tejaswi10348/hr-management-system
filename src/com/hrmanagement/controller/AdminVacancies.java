package com.hrmanagement.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.hrmanagement.bean.VacancyPojo;
/**
 * this class has the methods for serialization and deserialization of vacancies
 * @author Group H
 */
public class AdminVacancies {
	//This file is to store the vacancies.
	public static final String VACANCY_FILE = "vacancies.txt";
	//This is an object of type Array list to store the vacancies.
	static ArrayList<VacancyPojo> vList = new ArrayList<VacancyPojo>();
	/**
	 *This methood is to add vacancy details 
	 * @param vacancy object
	 */
	public void addVacancies(VacancyPojo vacancy) {
		//Fetching vacancy details from file
		vList = displayVacancies();
		//Checking if the file is empty or not
		if(vList.isEmpty()) {
			//Adding vacancy object to list
			vList.add(vacancy);
		}
		//Replacing the vacancy object if ithe list is not empty. 
		else {
			//setting the updated value to index zero
			vList.set(0,vacancy);
		}
        try 
        {
            FileOutputStream fout = new FileOutputStream(VACANCY_FILE);
            ObjectOutputStream out = new ObjectOutputStream(fout);
            //Writing object to list
            out.writeObject(vList);
            out.close();
            fout.close();
        } catch (Exception e) {}    
	}
	/**
	 * this method displays the vacancy details
	 * @return vacancies list
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<VacancyPojo> displayVacancies() {
		try 
	    {
			FileInputStream fin = new FileInputStream(VACANCY_FILE);
	        ObjectInputStream in = new ObjectInputStream(fin);
	        //Reading object from list
            vList = (ArrayList<VacancyPojo>)in.readObject();
	        in.close();
	        fin.close();
	    } catch (Exception e) {}
		return vList;	
	}
}
