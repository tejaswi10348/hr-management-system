package com.hrmanagement.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.hrmanagement.bean.EmployeePojo;
import com.hrmanagement.main.HrRecruitment;
/**
 * this class has the methods for serialization and deserialization of employee data
 * @author Group H
 *
 */
public class AdminEmployees {
	//This file is to store the employees.
	public static final String EMPLOYEE_FILE = "employeess.txt";
	//This is an object of type Array list to store the employees.
	static ArrayList<EmployeePojo> eList = new ArrayList<EmployeePojo>();
	/**
	 *this methood is to add employee details 
	 * @param employee object
	 */
	public void addEmployee(EmployeePojo employee) {
		//Fetching employee details from file
		eList = displayEmployees();
		//Adding employee object to list
		eList.add(employee);
        try {
            FileOutputStream fout = new FileOutputStream(EMPLOYEE_FILE);
            ObjectOutputStream out = new ObjectOutputStream(fout);
            //Writing object to list
            out.writeObject(eList);
            out.close();
            fout.close();
        } catch (Exception e) {}
	}
	
	
	/**
	 * this method displays the employee details
	 * @return employees list
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<EmployeePojo> displayEmployees() {
		try {
			FileInputStream fin = new FileInputStream(EMPLOYEE_FILE);
	        ObjectInputStream in = new ObjectInputStream(fin);
	        //Reading object from list
	        eList = (ArrayList<EmployeePojo>)in.readObject();
	        in.close();
	        fin.close();
		} catch (Exception e) {}
	return eList;
	}	
	/**
	 * this method is to remove employees from list
	 * @param 
	 * @return it return the list after removing that employee
	 */
	public  ArrayList<EmployeePojo> removeEmployee(String rId) {
		ArrayList<EmployeePojo> eList = displayEmployees(); 
		int length = eList.size();
		try{
			for(int i = 0;i < length;i++) {
				//Condition to remove the given ID from list.
			    if(eList.get(i).geteId().equals(HrRecruitment.getrId())){
			    	//Removing employee object from list
			    	eList.remove(i);
				    FileOutputStream fout = new FileOutputStream(EMPLOYEE_FILE);
				    ObjectOutputStream out = new ObjectOutputStream(fout);
				    //Adding employee object to list
				    out.writeObject(eList);
				    out.close();
				    fout.close();
			    }
			}
		}  catch(Exception e){}
		return eList;
	}	 
}
