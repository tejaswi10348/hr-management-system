package com.hrmanagement.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.hrmanagement.bean.FeedbackPojo;
/**
 * this class has the methods for serialization and deserialization of candidates feedback
 * @author Group H
 */
public class AdminFeedback {
	//This file is to store the feedback.
	public static final String FEEDBACK_FILE = "feedback.txt";
	//This is an object of type Array list to store the feedback.
	static ArrayList<FeedbackPojo> fList = new ArrayList<FeedbackPojo>();
	/**
	 *this methood is to add feedback details 
	 * @param feedback object
	 */
	public void addFeedback(FeedbackPojo feedback) {
		//Fetching vacancy details from file
		fList = displayFeedback();
		//Adding feedback object to list
		fList.add(feedback);
		try {
			FileOutputStream fout = new FileOutputStream(FEEDBACK_FILE);
		    ObjectOutputStream out = new ObjectOutputStream(fout);
		    //Writing object to list
		    out.writeObject(fList);
		    out.close();
		    fout.close();
		} catch (Exception e) {}	
	}

	/**
	 * this method is to display the feedback details
	 * @return feedback list
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<FeedbackPojo> displayFeedback() {
		try {
			FileInputStream fin = new FileInputStream(FEEDBACK_FILE);
		    ObjectInputStream in = new ObjectInputStream(fin);
		    //Reading object from list
		    fList = (ArrayList<FeedbackPojo>)in.readObject();
		    in.close();
		    fin.close();
		} catch (Exception e) {}
	return fList;
	}		
}
