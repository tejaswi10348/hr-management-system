package com.hrmanagement.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.hrmanagement.bean.TrainingPojo;
import com.hrmanagement.main.HrServices;
/**
 * this class has the methods for serialization and deserialization of employees training data
 * @author Group H
 *
 */
public class AdminTraining {
	//This file is to store the Training objects.
	public static final String TRAINING_FILE = "training.txt";
	//This is an object of type Array list to store the Training details.
	static ArrayList<TrainingPojo> tList = new ArrayList<TrainingPojo>();
	/**
	 *this methood is to add training details 
	 * @param training object
	 */
	public void addTrainingDetails(TrainingPojo trainee) {
		//Fetching employee details from file
		tList = displayTrainingDetails();
		//Adding vacancy object to list
		tList.add(trainee);
		try {
			FileOutputStream fout = new FileOutputStream(TRAINING_FILE);
			ObjectOutputStream out = new ObjectOutputStream(fout);
			//Writing object to list
			out.writeObject(tList);
			out.close();
			fout.close();
		} catch (Exception e) {}	
	}
	/**
	 * this method displays the training details
	 * @return training list
	 */ 
	@SuppressWarnings("unchecked")
	public ArrayList<TrainingPojo> displayTrainingDetails() {
		 try {
			 FileInputStream fin = new FileInputStream(TRAINING_FILE);
	         ObjectInputStream in = new ObjectInputStream(fin);
	         //Reading object from list
	         tList = (ArrayList<TrainingPojo>)in.readObject();
	         in.close();
	         fin.close();
	     } catch (Exception e) {}
		 return tList;
	}
	/**
	 * this method is to remove employees from training list
	 * @param 
	 * @return it return the list after removing that employee details
	 */
	public ArrayList<TrainingPojo> removeTrainingDetails(String rId) {
		ArrayList<TrainingPojo> tList = displayTrainingDetails(); 
		int length = tList.size();
		try{
		    for(int i = 0;i < length;i++) {
		    	if(tList.get(i).getEmpId().equals(HrServices.getrId()) ) {
		    		tList.remove(i);
			    	FileOutputStream fout = new FileOutputStream(TRAINING_FILE);
			    	ObjectOutputStream out = new ObjectOutputStream(fout);
			    	//adding a employee object to file
			    	out.writeObject(tList);
			    	out.close();
			    	fout.close();
		    	} 
		    }
		} catch(Exception e){}
		return tList;
	}
}
