package com.hrmanagement.main;

import java.util.Scanner;
/**
 * this class defines the login  part
 * @author Group H
 *
 */
public class Home {
	/**
	 * this method displays the Welcome message.
	 * @param args
	 */
	public static void main(String[] args) 
	{
		System.out.println("Welcome to Innominds");
		System.out.println("");
		// This method has portals for different users
		login();	
	}
	/**
	 * This method is a login portal where admin,employees and candidates can access.
	 */
	public static void login() {
		int choice = 0;
		EmployeeLogin eLogin = new EmployeeLogin();
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("choose your choice");
				System.out.println("1.Login as admin");
				System.out.println("2.Login as employee");
				System.out.println("3.Apply for job");
				System.out.println("===================================");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				choice=s.nextInt();
				if(choice == 1 || choice==2 || choice==3)
				flag = true;
			} catch(Exception e){
				System.out.println("Please enter vaid input");
				System.out.println("===================================");
				flag = false;
			}
		}
		switch(choice){
			case 1:
				//This method has two portals where HR and TR can access their accounts.
				adminLogin();
				break;
			case 2:
				// This method validates the login details of employee	
				eLogin.employeeValidation();
				break;
			case 3:
				//This method shows the functions of candidate 
				CandidateLogin.candidatePortal();
				break;
		}
	}
	/**
	 * This method is a login portal where HR and TR can access their accounts.
	 */
	public static void adminLogin() {
		int adminChoice=0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("choose");
				System.out.println("1.Login as HR");
				System.out.println("2.Login as TR");
				System.out.println("3.Back");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				adminChoice = s.nextInt();
				if(adminChoice == 1 || adminChoice==2 || adminChoice==3)
				flag = true;
			} catch(Exception e){
				System.out.println("Please enter vaid input");
				flag = false;
			}
		}
		switch(adminChoice) {
			case 1:
				//This method sets the HR data to object.
				HrLogin.hdata();
				//This method validates the login details of HR.
				HrLogin.hLogin();
				break;
			case 2:
				//This method sets the TR data to object.
				TrLogin.tdata();
				//This method validates the login details of HR.
				TrLogin.tLogin();
				break;
			case 3:
				//This method goes to home page
				login();
		}	
	}
}
