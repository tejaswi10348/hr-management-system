package com.hrmanagement.main;

import java.util.ArrayList;
import java.util.Scanner;

import com.hrmanagement.bean.EmployeePojo;
import com.hrmanagement.bean.TrainingPojo;
import com.hrmanagement.controller.AdminEmployees;
import com.hrmanagement.controller.AdminTraining;

/**
 *This class contains the employee portal
 *@author Group H
 */
public class EmployeeLogin {
	//Employee details are stored in this file.
	public static final String EMPLOYEE_FILE = "employeess.txt";
	//This is object of type Array list to store the employees.
	static ArrayList<EmployeePojo> eList = new ArrayList<EmployeePojo>();
	//Training details are stored in this file.
	public static final String TRAINING_FILE = "training.txt";
	//This is object of type Array list to store the traininig details.
	static ArrayList<TrainingPojo> tList = new ArrayList<TrainingPojo>();
	//This is object of type Scanner to store the user inputs.
	static Scanner s = new Scanner(System.in);
	/**
	 *This method is to validate employee login details.
	 */
	@SuppressWarnings({ "unused", "static-access" })
	public void employeeValidation() {
		AdminEmployees aEmployee = new AdminEmployees();  
		String eid = null;
			boolean flag = false;
		    while(!flag) {
		    	System.out.println("Enter employeeid: ");
		    	eid = s.next();
		    	System.out.println("Enter password: ");
		    	String ePassword = s.next();
		    	//Employee details in file are going to store in eList. 
		    	eList = aEmployee.displayEmployees(); 
		    	String empid;
		    	int length = eList.size();
				for(int i = 0;i <length;i++) {
					//Checking condition for userId and password
					if(eList.get(i).geteId().equals(eid) && eList.get(i).getePassword().equals(ePassword)) {
						System.out.println("===================================");
						System.out.println("STATUS: Login successfull");
						System.out.println("===================================");
						System.out.println("           Hi "+eList.get(i).geteName());
						System.out.println("           ---------");
						//This method displays the training details of employees 
						employeeServices(eid);
						flag = true;
					}	
				}	
			}
		    if(flag == false) {
				System.out.println("Wrong");
			}	
	}
	/**
	 * This method displays the training details to employees. 
	 * @param eid
	 */
	public void employeeServices(String eid) {
		AdminTraining aTraining = new AdminTraining();
		//Employee training details from training file are going to store in eList. 
		tList = aTraining.displayTrainingDetails();
		int employeeChoice = 0;
		int length1=tList.size();
		for (int j = 0; j < length1; j++) {
			//Condition for checking the employee id in training list
			if ( tList.get(j).getEmpId().equals(eid)) {
				double percentage = tList.get(j).getePercentage();
				double attendance = tList.get(j).geteAttendance();
				double payment = tList.get(j).geteSalary();
				boolean flag1 = false;
				while(!flag1) {
					try {
						System.out.println("Choose");
						System.out.println("1.Assessment details");
						System.out.println("2.Attendance");
						System.out.println("3.Slary details");
						System.out.println("4.Logout");
						@SuppressWarnings("resource")
						Scanner s=new Scanner(System.in);
						employeeChoice = s.nextInt();
						System.out.println("===================================");
						if(employeeChoice == 1 || employeeChoice == 2 || employeeChoice == 3 || employeeChoice == 4 ) 
						flag1=true;
					} catch(Exception e) {
						System.out.println("Please enter vaid input");
						flag1 = false;
					}
				}
				switch(employeeChoice) {
					case 1:
						System.out.println("Assessment average: "+percentage+"%");
						System.out.println("===================================");
						employeeServices(eid);
						break;
					case 2:
						System.out.println("Attendance: "+attendance+"%");
						System.out.println("===================================");
						employeeServices(eid);
						break;
					case 3:	
						System.out.println("Slary: "+payment+" CTC per annum");
						System.out.println("===================================");
						employeeServices(eid);
						break;
					case 4:
						Home.login();
				}
			}
		}	
	}	
}



