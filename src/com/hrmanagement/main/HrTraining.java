package com.hrmanagement.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.hrmanagement.bean.EmployeePojo;
import com.hrmanagement.bean.TrainingPojo;
import com.hrmanagement.controller.AdminEmployees;
import com.hrmanagement.controller.AdminTraining;
/**
 * This class contains the training functions of HR 
 * @author Group H 
 */
public class HrTraining {
	//This is object of type Array list to store the training details of employees.
	static ArrayList<TrainingPojo> tList = new ArrayList<TrainingPojo>();
	//Training details are stored in this file.
	public static final String TRAINING_FILE = "training.txt";
	//This is object of type Array list to store the employees.
	static ArrayList<EmployeePojo> eList = new ArrayList<EmployeePojo>();
	//Employee details are stored in this file.
	public static final String EMPLOYEE_FILE = "employeess.txt";
	AdminEmployees aEmployee = new AdminEmployees();
	AdminTraining aTraining = new AdminTraining();
	EmployeePojo employee = new EmployeePojo();
	TrainingPojo training = new TrainingPojo();
	Scanner s= new Scanner(System.in);
	
	/**
	 * This method shows the different services in training.
	 */
	@SuppressWarnings("static-access")
	public void trainingDetails() {
		boolean flag = false;
		System.out.println("===================================");
		System.out.println("Employees list");
		//this method displays the list of employees in company 
		aEmployee.displayEmployees();
		//Fetching employee details in eList
		eList = AdminEmployees.displayEmployees();
		//Iterator to display the employee details.
	    Iterator<EmployeePojo> itr1=eList.iterator();  		
	    while(itr1.hasNext()){  			
	        EmployeePojo employeesList = (EmployeePojo) itr1.next();
	 		System.out.println(employeesList);  
	 	}
        System.out.println("===================================");
	    System.out.println("Upload Training details");
		
	    // checking employee id
	    while(!flag) {
			System.out.println("Enter Employee ID:");
			String id = s.next();
			int length = eList.size();
			//Fetching employee details in eList
			eList = aEmployee.displayEmployees();
			for (int i = 0 ; i < length ; i++) {
				//Condition for checking the employee id is there in employees list or not.
				if(eList.get(i).geteId().equals(id)) {
					training.setEmpId(id);
					flag = true;
				}	
			}if(flag == false) {
				System.out.println("Entered employee Id does not exits in employees list.");
			}
		}
		
	    //employee domain
	    flag = false;
		while(!flag) {
			System.out.println("Enter the domain of employee (java/javascript):");
		    String domain = s.next();
			if (domain.equals("java")|| domain.equals("javascript")) {
				 training.seteDomain(domain);
				break;
			} else {
				System.out.println("Please Enter only the above domains");
			}
		}
	    
	    System.out.println("Enter the marks of first Assessment");
	    int assessment1 = s.nextInt();
	    System.out.println("Enter the marks of second Assessment");
	    int assessment2 = s.nextInt();
	    System.out.println("Enter the marks of third Assessment");
	    int assessment3 = s.nextInt();
	    int total=(assessment1+assessment2+assessment3);
	    double percentage = (100*total)/300;
	    training.setePercentage(percentage);
	    
	    System.out.println("Enter the employee's attendance percentage");
	    double attendance = s.nextDouble();
	    training.seteAttendance(attendance);
	    
	    double payment = 230000;
	    if(percentage>85 && attendance>85) {
	    	payment = 250000 ;
	    } else if((percentage>70 && percentage<85) && (attendance>70 && attendance <85)) {
	    	payment = 240000 ;
	    } else {
	    	payment = 230000;
	    }
	    training.seteSalary(payment);
	    
	    //This method adds training object to list 
	    aTraining.addTrainingDetails(training);
	    System.out.println("");
		System.out.println("Employee details added successfully");
	}
	/**
	 * this method checks whether the given id is in employees list or not
	 * @param id
	 */
	@SuppressWarnings("static-access")
	public void idCheck(String id) {
		//Fetching employee details in eList
		eList = aEmployee.displayEmployees();
		int length = eList.size();
		try {
   	    	for(int i = 0;i<length;i++){
   	    		//Condition for checking the employee id is present in list or not.
   	    		if(!eList.get(i).geteId().equals(id)){
   	    			System.out.println("Employee not exists");
   	    		}
   	    		else
   	    			break;
   	    	}
    	} catch(Exception e) {}		
	}	
}
