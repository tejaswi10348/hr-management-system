package com.hrmanagement.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hrmanagement.bean.CandidatePojo;
import com.hrmanagement.bean.EmployeePojo;
import com.hrmanagement.bean.FeedbackPojo;
import com.hrmanagement.bean.VacancyPojo;
import com.hrmanagement.controller.AdminVacancies;
import com.hrmanagement.controller.AdminFeedback;
import com.hrmanagement.controller.AdminTraining;
import com.hrmanagement.controller.AdminEmployees;
/**
 * This class contains the different functions of HR  
 * @author Group H
 */
public class HrRecruitment {
	//Employee details are stored in this file.
	public static final String EMPLOYEE_FILE = "employeess.txt";
	//This is object of type Array list to store the employees.
	static ArrayList<EmployeePojo> eList = new ArrayList<EmployeePojo>();
	//Candidates details are stored in this file
	public static final String CANDIDATE_FILE = "candidates.txt";
	//This is object of type Array list to store the candidates
	static ArrayList<CandidatePojo> cList = new ArrayList<CandidatePojo>();
	//Vacancy details are stored in this file
	public static final String VACANCY_FILE = "vacancies.txt";
	//This is object of type Array list to store the vacancies
	static ArrayList<VacancyPojo> vList = new ArrayList<VacancyPojo>();
	//Feedback details are stored in this file
	public static final String FEEDBACK_FILE = "feedback.txt";
	//This is object of type array list to store the feedback
	static ArrayList<FeedbackPojo> fList = new ArrayList<FeedbackPojo>();
	//Regex pattern for validation
	static final String NAME_REGEX ="^[a-zA-Z]{3,12}$";
	static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
	static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,10})";
	static final String PHONE_REGEX ="^\\d{10}$";
	static final String MARKS_REGEX ="^\\d{1,3}$";
	static String rId;
	public static String getrId() {
		return rId;
	}
	public static void setrId(String rId) {
		HrRecruitment.rId = rId;
	}
	AdminEmployees aEmployee = new AdminEmployees(); 
	VacancyPojo vacancy = new VacancyPojo();
	AdminVacancies aVacancy = new AdminVacancies();
	AdminTraining aTraining = new AdminTraining();
	EmployeePojo employee = new EmployeePojo();
	HrServices hService= new HrServices();
	Scanner s = new Scanner(System.in);
	/**
	 * This method shows the duties of admin
	 */
	@SuppressWarnings("static-access")
	public void recruitment() {
		boolean flag = false;
		int choice = 0 ;
		while(!flag) {
			try {
				System.out.println("===================================");
				System.out.println("choose ");
				System.out.println("1.Display vacancies");
				System.out.println("2.Add vacancies");
				System.out.println("3.Remove vacancies");
				System.out.println("4.Display employees");
				System.out.println("5.Add employees");
				System.out.println("6.Remove Employees");
				System.out.println("7.Show new candidates");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				choice = s.nextInt();
				if(choice == 1 ||choice == 2 ||choice == 3 ||choice == 4 ||choice == 5 ||choice == 6 ||choice == 7) 
					flag = true;
			} catch(Exception e) {
				System.out.println("Please enter vaid input");
				flag = false;
			}
		}
		switch(choice){
			case 1:
				try {
					vList = new ArrayList<VacancyPojo>();
					vList = aVacancy.displayVacancies();
					if(!vList.isEmpty()) {
						//this method displays the number of vacancies in company 
						
						System.out.println("");
						//checking if the vacancies in the list is zero 
						if(vList.get(0).getVacancy().equals(0)) {
							System.out.println("No vacancies");
						} else {
							System.out.println("vacancies in company:"+vList.get(0).getVacancy());
						}
					} else {
						System.out.println("No vacancies");
						
					}
					navigation();
				} catch(Exception ex) {}
				break;
			case 2:
				try {
					System.out.print("Create vancancies[Enter only numbers]:");
					//asking user to enter the vacancies in number
					int vacancies = s.nextInt();
					vList = aVacancy.displayVacancies();
					if(vList.isEmpty()) {
						vacancy.setVacancy(vacancies);
						//This method is to store vacancies in the list
						aVacancy.addVacancies(vacancy);
						//Fetching vacancy details in vList
						vList = aVacancy.displayVacancies();
						System.out.println("");
						System.out.println("Vacancies:"+vacancies);	
					}
					else {
						int previousVacancies=0;
						previousVacancies= vList.get(0).getVacancy();
						System.out.println("Previous vacancies: "+previousVacancies);
						vacancies = previousVacancies+vacancies;			
						//storing the vacancies in vacancy object
						vacancy.setVacancy(vacancies);
						//This method is to store vacancies in the list
						aVacancy.addVacancies(vacancy);
						//Fetching vacancy details in vList
						vList = aVacancy.displayVacancies();
						System.out.println("");
						System.out.println("Updated vacancies:"+vacancies);
					} 
					navigation();
				} catch(Exception ex) {}
				break;
			case 3:
				//This method is to remove vacancies in the list
				removeVacancies();
				break;
			case 4: 
				try {
					System.out.println("===================================");
					System.out.println("EMPLOYEES LIST");
					//This method displays the list of employees in company 
					aEmployee.displayEmployees();
					eList = aEmployee.displayEmployees();
					//it is used for iterating the employees list 
			    	Iterator<EmployeePojo> itr=eList.iterator();  		
			        while(itr.hasNext()){  			
			        	EmployeePojo employeesList = (EmployeePojo) itr.next();
			 		   	System.out.println(employeesList);  
			 		} 
					navigation();
				} catch (Exception ex) {}
				break;
			case 5:
				//This method stores employee details in an object
				employeeDetails();
				break;
			case 6:
				try {
					System.out.println("enter the Id of employee to remove him from organization");
					rId = s.next();
					setrId(rId);
					//This method removes employee details from list
					aEmployee.removeEmployee(rId);
					hService.setrId(rId);
					//This method removes employee training details from list
					aTraining.removeTrainingDetails(rId);
					System.out.println("Employee removed successfully");
					navigation();
				} catch (Exception ex) {}
				break;
			case 7:
				try {
					System.out.println("===================================");
					ArrayList<FeedbackPojo> fList=AdminFeedback.displayFeedback();	
					System.out.println("Candidates who cleared the technical interview");
					System.out.println("DOMAIN:JAVA");
					int length = fList.size(); 
					for(int i=0;i<length;i++) {
						//this condition is to checkthe candidates who passed in java domain
						if(fList.get(i).getResult().equals("yes") && fList.get(i).getDomain().equals("java")) {
							System.out.println(fList.get(i));
						}
					}
					System.out.println("");
					System.out.println("-----------------------------------");
					System.out.println("DOMAIN:JAVASCRIPT");
					for(int i=0;i<length;i++) {
						//this condition is to checkthe candidates who passed in javaScript domain
						if(fList.get(i).getResult().equals("yes") && fList.get(i).getDomain().equals("javascript")) {
							System.out.println(fList.get(i));
						}
					} 
					navigation();
				} catch (Exception ex) {
					System.out.println("Please enter vaid input");
				}
				break;
			}	
	}
	/**
	 *This method is to remove vacancies.
	 */
	@SuppressWarnings("static-access")
	public void removeVacancies() {
		try {
			//Fetching vacancy details in vList
			vList = aVacancy.displayVacancies();
			int previousVacancies = vList.get(0).getVacancy();
			System.out.println("Previous vacancies: "+previousVacancies);
			System.out.print("Enter vacancies to remove:");
			//asking user to enter the vacancies in number
			int vacancies = s.nextInt();
			if(previousVacancies>=vacancies) {
				vacancies = previousVacancies-vacancies;
				//storing the vacancies in vacancy object
				vacancy.setVacancy(vacancies);
				//calling addVacancies method and there the object is stored
				aVacancy.addVacancies(vacancy);
				vList = aVacancy.displayVacancies();
				System.out.println("");
				System.out.println("Updated vacancies:"+vacancies); 
				navigation();
			}
			else {
				System.out.println("Can't remove vacanceies.Please check once again");
				removeVacancies();
			}
		} catch(Exception ex) {
			System.out.println("Please enter vaid input");
		}
	}
	/**
	 *this method stores the employee details in an object 
	 */
    @SuppressWarnings("static-access")
	public void employeeDetails() {
    	System.out.println("===================================");
    	AdminFeedback aFeedback = new AdminFeedback();
    	System.out.println("Candidates who cleared the Technical Interview");
    	//Fetching feedback details in fList
    	ArrayList<FeedbackPojo> fList=aFeedback.displayFeedback();
    	int length = fList.size(); 
		for(int i=0;i<length;i++) {
			//Condition to get the candidates who passed in TR (i.e.,status "yes")
			if(fList.get(i).getResult().equals("yes")) {
				System.out.println(fList.get(i));	
			}
		}
		System.out.println("===================================");
    	boolean flag = false;
    	String email;
    	String password;
    	String phNumber;
    	System.out.println("EMPLOYEE REGISTRATION");
		
    	
    	System.out.println("Enter Employee ID:");
		String id = s.next();
		//This method checks whether the employee is getting unique id or not.
		idcheck(id);
		employee.seteId(id);
		
		//employee name
		while(!flag) {
			System.out.println("Enter Name:");
			String name = s.next();
			int length1 = fList.size();
			fList = aFeedback.displayFeedback();
			for (int i = 0 ; i < length1 ; i++) {
				// Condition for checking whether the name is there in feedbacklist or not.
				if(fList.get(i).getName().equals(name)) {
					employee.seteName(name);
					flag = true;
				}	
			}if(flag == false) {
				System.out.println("Employyee name not exits in Feedback list.");
			}
		}
		
		//employee emailId
		flag = false;
		while(!flag) {
			System.out.println("Enter Employee emailId:");
			email = s.next();
			//This method checks the user given email id with regex(email)
			flag = mailCheck(EMAIL_REGEX,email);
			if(flag) {
				employee.seteEmail(email);
			} else {
				System.out.println("ALERT:Please Enter valid emailId");
				System.out.println("-----------------------------------");
			}
		}
		// employee password
		flag = false;
		while(!flag) {
			System.out.println("Enter Employee Password:");
			password = s.next();
			//This method checks the user given password id with regex(password)
			flag = passwordCheck(PASSWORD_REGEX,password);
			if(flag) {
				employee.setePassword(password);
			} else {
				System.out.println("ALERT:Please Enter valid Password.It should contain atleast 1 uppercase,1 lowercase and 1 digit with minimum size 6");
				System.out.println("-----------------------------------");
			}
		}
		
		
		flag = false;
		while(!flag) {
			System.out.println("Enter Employee phone number:");
			phNumber = s.next();
			//This method checks the user given phone number with regex(phone number)
			flag = mobileCheck(PHONE_REGEX,phNumber);
			if(flag) {
				employee.setePhNumber(phNumber);
			} else {
				System.out.println("ALERT:Please Enter valid Phone number.It should contain 10 digits");
				System.out.println("-----------------------------------");
			}
		}
		//This method adds the employee object to list 
		aEmployee.addEmployee(employee);
		System.out.println("");
		System.out.println("Employee added successfully");
		try { 
			navigation();
		} catch (Exception e1) {}	
    }
    
    
    /**
     * this method checks whether the given id is present in existing employees list
     * @param id
     */
    @SuppressWarnings("static-access")
	public void idcheck(String id) {
    	//Fetching employee details in eList
		ArrayList<EmployeePojo> eList = aEmployee.displayEmployees();
    	int length = eList.size();
    	try {
    		boolean flag = false;
   	    	for(int i = 0;i<length;i++){
   	    		//Condition for checking the unique id.
   	    		if(eList.get(i).geteId().equals(id)){
   	    			System.out.println("Employee id already exists");
   	    			System.out.println("===================================");
   	    			flag = true;
   	    			break;
   	    		}
   	    	}
   	    	if(flag){
   	    		employeeDetails();
   	    	}
    	} catch(Exception e) {
    		System.out.println("Please enter vaid input");
    	}		
	}
    
    /**
   	 * this method checks the name with given regex pattern
   	 * @param NAME_REGEX
   	 * @param name
   	 * @return boolean
   	 */
   	public boolean nameCheck(String NAME_REGEX,String name) {
    		 Pattern eRegex;
   		 Matcher eName;	
   		 eRegex = Pattern.compile(NAME_REGEX);
   		 eName = eRegex.matcher(name);
   	return  eName.matches();   
    	}
    
    
    /**
	 * this method checks the email with given regex pattern
	 * @param EMAIL_REGEX
	 * @param mail
	 * @return boolean
	 */
	public boolean mailCheck(String EMAIL_REGEX,String mail) {
 		 Pattern eRegex;
		 Matcher eEmail;	
		 eRegex = Pattern.compile(EMAIL_REGEX);
		 eEmail = eRegex.matcher(mail);
	return  eEmail.matches();   
 	}
	
	/**
	 * this method checks the password with given regex pattern
	 * @param PASSWORD_REGEX
	 * @param password
	 * @return boolean
	 */
	public boolean passwordCheck(String PASSWORD_REGEX,String password) {
		 Pattern pRegex;
		 Matcher pEmail;	
		 pRegex = Pattern.compile(PASSWORD_REGEX);
		 pEmail = pRegex.matcher(password);
	return  pEmail.matches();   
	}

	/**
	 *this method checks the mobile number with given regex pattern 
	 * @param PHONE_REGEX
	 * @param phNumber
	 * @return boolean
	 */
	public boolean mobileCheck(String PHONE_REGEX,String phNumber) {
		 Pattern pnRegex;
		 Matcher phno;	
		 pnRegex = Pattern.compile(PHONE_REGEX);
		 phno = pnRegex.matcher(phNumber);
	return  phno.matches();   
	}
	/**
	 *this method checks the marks with given regex pattern 
	 * @param MARKS_REGEX
	 * @param marks
	 * @return boolean
	 */
	public boolean marksCheck(String MARKS_REGEX, String marks) {
		Pattern cMarksRegex;
		Matcher cMarks;	
		cMarksRegex = Pattern.compile(MARKS_REGEX);
		cMarks = cMarksRegex.matcher(marks);
		return cMarks.matches();
	}	
	
	/**
	 * This method is for internal navigation
	 */
	public void navigation(){	
		HrServices hService = new HrServices();
		boolean flag = false;
		int adminReturn = 0;
		while(!flag) {
			try {
				System.out.println("");
				System.out.println("===================================");
				System.out.println("press 1 for going back to recruitment");
				System.out.println("press 2 for going back to training");
				System.out.println("===================================");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				adminReturn = s.nextInt();
				if ((adminReturn == 1)||(adminReturn == 2))
					flag = true;
			} catch(Exception e) {
				System.out.println("Please enter vaid input");
				flag = false;
			}
		}
		switch(adminReturn){
		case 1:
			recruitment();
			break;
		case 2:	
			hService.hrFunctions();
			break;
		}		
	}
}