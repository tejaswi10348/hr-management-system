package com.hrmanagement.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.hrmanagement.bean.EmployeePojo;
import com.hrmanagement.bean.TrainingPojo;
import com.hrmanagement.controller.AdminEmployees;
import com.hrmanagement.controller.AdminTraining;
/**
 * this class contains the different services of HR  
 * @author Group H
 */
public class HrServices {
	//Employee details are stored in this file
	public static final String EMPLOYEE_FILE = "employeess.txt";
	//Training details are stored in this file
	public static final String TRAINING_FILE = "training.txt";
	//This is object of type Array list to store the training details
	static ArrayList<TrainingPojo> tList = new ArrayList<TrainingPojo>();
	//This is object of type Array list to store the employees
	static ArrayList<EmployeePojo> eList = new ArrayList<EmployeePojo>();
	AdminEmployees aEmployee=new AdminEmployees();
	HrTraining hTraining = new HrTraining();
	EmployeePojo employee = new EmployeePojo();
	TrainingPojo training = new TrainingPojo();
	//This is object of type Scanner to store the user inputs.
	Scanner s = new Scanner(System.in);
	static String rId;
	public static String getrId() {
		return rId;
	}
	public static void setrId(String rId) {
		HrServices.rId = rId;
	}
	/**
	 * this method shows the different services HR can perform
	 */
	public void hrFunctions() {
		HrRecruitment hRecruitment = new HrRecruitment();
		int choice = 0;
		boolean flag = false;
		while(!flag){
			try {
				System.out.println("choose your service");
				System.out.println("1.Recruitment");
				System.out.println("2.Training");
				System.out.println("3.Logout");
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				choice = s.nextInt();
				if(choice == 1 || choice==2 || choice==3)
				flag=true;
			}catch(Exception e) {
				System.out.println("please enter valid input");
				flag= false;
			}
		}
		switch(choice){
			case 1: 
				hRecruitment.recruitment();
				break;
			case 2:
				trainingFunctions();
				break;
			case 3:
				Home.adminLogin();
				break;
		}
	}
	/**
	 * this method is CRUD operations of training details.
	 */
	public void trainingFunctions() {
		AdminTraining aTraining = new AdminTraining();
		boolean flag= false;
		int choice1 = 0;
		while(!flag) {
			try {
				System.out.println("===================================");
				System.out.println("choose your service");
				System.out.println("1.Add training details");
				System.out.println("2.Display Training Details");
				System.out.println("3.Remove Training Details");
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				choice1 = s.nextInt();	
				if(choice1 == 1 || choice1==2 || choice1==3)
					flag = true;
			} catch(Exception e) {
				System.out.println("Please enter vaid input");
				flag = false;
			}
		}
		switch(choice1){
		case 1:
			//This method is to add training details to list 
			hTraining.trainingDetails();
			backup();
		case 2:
			try {
				System.out.println("===================================");
				System.out.println("Training details of students");
				//This method displays the training details 
				aTraining.displayTrainingDetails();
				//This method is to display training details 
				tList = aTraining.displayTrainingDetails();
		    	Iterator<TrainingPojo> itr = tList.iterator();  		
		        while(itr.hasNext()){  			
		        	TrainingPojo trainingList = (TrainingPojo) itr.next();
		 		   	System.out.println(trainingList);  
		 		}
		        backup();
			} catch (Exception ex) {}	
			break;
		case 3:
			try {
				System.out.println("===================================");
				System.out.println("Training list"); 
				aTraining.displayTrainingDetails(); 
				tList = aTraining.displayTrainingDetails();
				//Iteraator to teraye the training details list
		    	Iterator<TrainingPojo> itr2 = tList.iterator();  		
		        while(itr2.hasNext()){  			
		        	TrainingPojo trainingList = (TrainingPojo) itr2.next();
		 		   	System.out.println(trainingList);  
		 		}
		        System.out.println("===================================");
				System.out.println("enter the Id to remove his details");
				rId = s.next();
				setrId(rId);
				//This method removes the training details of an employee from list.
				aTraining.removeTrainingDetails(rId);
				System.out.println("Employee removed successfully");
				backup();
			} catch (Exception ex) {
				System.out.println("Please enter vaid input");
			}
			break;
		} 
	}
	
	/**
	 * This method is for internal navigation
	 */
	public void backup(){	
		HrServices hService = new HrServices();
		boolean flag = false;
		int adminReturn = 0;
		while(!flag) {
			try {
				System.out.println("");
				System.out.println("===================================");
				System.out.println("press 1 for going back to Training");
				System.out.println("press 2 for going back to HR functions");
				System.out.println("===================================");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				adminReturn = s.nextInt();
				if ((adminReturn == 1)||(adminReturn == 2))
					flag = true;
			} catch(Exception e) {
				System.out.println("Please enter vaid input");
				flag = false;
			}
		}
		switch(adminReturn){
		case 1:
			trainingFunctions();
			break;
		case 2:	
			hService.hrFunctions();
			break;
		}		
	}	
}
