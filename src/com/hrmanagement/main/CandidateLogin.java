package com.hrmanagement.main;

import java.util.ArrayList;
import java.util.Scanner;

import com.hrmanagement.bean.CandidatePojo;
import com.hrmanagement.bean.VacancyPojo;
import com.hrmanagement.controller.AdminCandidates;
import com.hrmanagement.controller.AdminVacancies;
/**
 * This class contains the login part of candidates.
 * @author Group H
 */
public class CandidateLogin {
	//Vacancies details are stored in this file.
	public static final String VACANCY_FILE = "vacancies.txt";
	//This is object of type Array list to store the vacancies.
	static ArrayList<VacancyPojo> vList = new ArrayList<VacancyPojo>();
	//Candidates details are stored in this file
	public static final String CANDIDATE_FILE = "candidates.txt";
	//creating the object of type Array list to store the candidates
	static ArrayList<CandidatePojo> cList = new ArrayList<CandidatePojo>();
	static AdminVacancies aVacancy = new AdminVacancies();
	static HrRecruitment hRecruitment = new HrRecruitment();
	//Regex pattern for input fields
	static final String NAME_REGEX ="^[a-zA-Z]{3,12}$";
	static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
	static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,10})";
	static final String PHONE_REGEX ="^\\d{10}$";
	static final String MARKS_REGEX ="^\\d{1,3}$";
	static Scanner s = new Scanner(System.in);
	/**
	 * Candidates portal
	 */
	@SuppressWarnings("static-access")
	public static void candidatePortal() {
		int candidateChoice=0;
		boolean flag= false;
		while(!flag) {
			try {
				System.out.println("choose");
				System.out.println("1.Show vacancies in the company");
				System.out.println("2.Apply ");
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				candidateChoice = s.nextInt();
				if(candidateChoice == 1 || candidateChoice == 2)
				flag=true;
			} catch(Exception e) {
				System.out.println("Please enter vaid input");
				System.out.println("==================================="); 
				flag = false;
			}
		}
		//This is object of type CandidatePojo to store the candidate details.
		CandidatePojo candidate = new CandidatePojo();
		AdminCandidates aCandidate = new AdminCandidates();
		switch(candidateChoice) {
			case 1:
				try {
					vList = new ArrayList<VacancyPojo>();
					vList = aVacancy.displayVacancies();
					if(vList.isEmpty()) {
						System.out.println("No vacancies");
					} else {
						//This method displays the number of vacancies in company 
						vList = aVacancy.displayVacancies();
						System.out.println("");
						//Checking if the vacancies in the list is zero 
						if(vList.get(0).getVacancy().equals(0)) {
							System.out.println("No vacancies");
						} else {
							System.out.println("vacancies in company:"+vList.get(0).getVacancy());
						}
					} 
					System.out.println("==================================="); 
					candidatePortal();
				} catch(Exception ex) {}
				break;
			case 2:
				boolean flag1 = false;
				try {
					System.out.println("Application process");
					
					//Candidate name
					while(!flag1) {
						System.out.println("Enter your Name:");
						String name = s.next();
						flag1 = hRecruitment.nameCheck(NAME_REGEX,name);
						if(flag1) {
							candidate.setcFullName(name);
						} else {
							System.out.println("ALERT:Please Enter valid name.It should contain only alphabets.");
						}
					}
					
					//Candidate phone number
					flag1 = false;
					while(!flag1) {
						System.out.println("Enter your Phone number:");
						String phoneNumber = s.next();
						flag1 = hRecruitment.mobileCheck(PHONE_REGEX,phoneNumber);
						if(flag1) {
							candidate.setcPhNumber(phoneNumber);
						} else {
							System.out.println("ALERT:Please Enter valid Phone number.It should contain 10 digits");
						}
					}
					
					//Candidate emailid
					flag1 = false;
					while(!flag1) {
						System.out.println("Enter your email_id:");
						String mail = s.next();
						flag1 = hRecruitment.mailCheck(EMAIL_REGEX,mail);
						if(flag1) {
							candidate.setcEmail(mail);
						} else {
							System.out.println("Please Enter valid emailId");
						}
					}
					
					//candidate's ssc percentage
					flag1 = false;
					while(!flag1) {
						System.out.println("Enter your SSC percentage:");
						String ssc = s.next(); 
						flag1 = hRecruitment.marksCheck(MARKS_REGEX,ssc);
						if(flag1) {
							candidate.setcXAggregate(ssc);
						} else {
							System.out.println("Please Enter valid marks");
						}
					}
					
					//candidate's XII percentage
					flag1 = false;
					while(!flag1) {
						System.out.println("Enter your XII percentage:");
						String inter = s.next();
						flag1 = hRecruitment.marksCheck(MARKS_REGEX,inter);
						if(flag1) {
							candidate.setcXIIAggregate(inter);
						} else {
							System.out.println("Please Enter valid marks");
						}
					}
					//candidate's highest qualification
					System.out.println("Enter your Highest qualification:");
					String hqual = s.next(); 
					candidate.setcHighestQualification(hqual);
					
					//candidate's highest qualification percentage
					flag1 = false;
					while(!flag1) {
						System.out.println("Enter your percentage:");
						String per = s.next();
						flag1 = hRecruitment.marksCheck(MARKS_REGEX,per);
						if(flag1) {
							candidate.setcHPercentage(per);
						} else {
							System.out.println("Please Enter valid marks");
						}
					}
					
					//candidate's domain
					flag1 = false;
					while(!flag1) {
						System.out.println("Enter the domain you want to apply for (java/javascript):");
						String domain = s.next(); 
						if (domain.equals("java")||domain.equals("javascript")) {
							candidate.setcDomain(domain);
							break;
						} else {
							System.out.println("Please Enter only the above domains");
						}
					}
					
				//This method is to add candidate to list
				aCandidate.addCandidate(candidate);
				System.out.println("");
				System.out.println("STATUS:Applied successfully");
				System.out.println("");
				System.out.println("===================================");
			} catch (Exception e) {}
			break;
		}	
	}	 
}



