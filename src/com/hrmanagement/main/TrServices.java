package com.hrmanagement.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.hrmanagement.bean.CandidatePojo;
import com.hrmanagement.bean.FeedbackPojo;
import com.hrmanagement.controller.AdminCandidates;
import com.hrmanagement.controller.AdminFeedback;
/**
 * This class contains the TR services
 * @author Group H
 */ 
public class TrServices {
	Scanner s=new Scanner(System.in);
	FeedbackPojo feedback = new FeedbackPojo();
	//Feedback details are stored in this file.
	public static final String FEEDBACK_FILE = "feedback.txt";
	//This is object of type Array list to store the feedback.
	static ArrayList<FeedbackPojo> fList = new ArrayList<FeedbackPojo>();
	//Candidate details are stored in this file.
	public static final String CANDIDATE_FILE = "candidates.txt";
	//This is object of type Array list to store the candidates.
	static ArrayList<CandidatePojo> cList = new ArrayList<CandidatePojo>();
	static final String NAME_REGEX ="^[a-zA-Z]{3,12}$";
	AdminFeedback aFeedback = new AdminFeedback();
	AdminCandidates aCandidate = new AdminCandidates();
	HrRecruitment hRecruitment = new HrRecruitment();
	/**
	 * This method is about TR functions.
	 */
	@SuppressWarnings("static-access")
	public void trFunctions() {
		int trChoice=0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("choose");
				System.out.println("1.Applied candidates");
				System.out.println("2.Interview feedback");
				System.out.println("3.Logout");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				trChoice = s.nextInt();
				if (trChoice == 1 || trChoice == 2 || trChoice==3) 
					flag = true;
			}catch(Exception e) {
				System.out.println("Please enter vaid input");
				System.out.println("===================================");
				flag = false;
			}
		}
		switch (trChoice) {
			case 1:
				System.out.println("===================================");
				System.out.println("APPLIED CANDIDATES");
				//Fetching candidates details in cList
				cList = aCandidate.displayCandidates();
				//Iterator to display candidate details.
				Iterator<CandidatePojo> it = cList.iterator();  		
				while(it.hasNext()){  			
					CandidatePojo candidatesList = (CandidatePojo) it.next();
				   	System.out.println(candidatesList);  
				}
				navigation();
			break;
			case 2:
				boolean flag1 = false;
				System.out.println("===================================");
				System.out.println("CANDIDATES FEEDBACK");
				//Fetching feedback details in fList
				fList = aFeedback.displayFeedback();
				//Iterator to desplay candidate details.
				Iterator<FeedbackPojo> it1 = fList.iterator();  		
				while(it1.hasNext()){  			
					FeedbackPojo feedbackList = (FeedbackPojo) it1.next();
				   	System.out.println(feedbackList);  
				}
				System.out.println("===================================");
				System.out.println("GIVE FEEDBACK");
				
				//Candidates name
				while(!flag1) {
					System.out.println("Enter Name:");
					String name = s.next();
					int length = cList.size();
					//Fetching candidates details in cList
					cList = aCandidate.displayCandidates();
					for (int i = 0 ; i < length ; i++) {
						//Condition for checking the candidate name is in applied(candidates) list or not.
						if(cList.get(i).getcFullName().equals(name)) {
							feedback.setName(name);
							flag1 = true;
						}	
					}if(flag1 == false) {
						System.out.println("Entered name not exits in applied candidates list.");
					}
				}
				
				//candidate domain
				flag1 = false;
				while(!flag1) {
					System.out.println("Enter Domain:");
					String domain = s.next(); 
					if (domain.equals("java")||domain.equals("javascript")) {
						feedback.setDomain(domain);
						break;
					} else {
						System.out.println("Please Enter only the above domains");
					}
				}
					
				//candidate status
				flag1 = false;
				while(!flag1) {
					System.out.println("Enter status of intervieview process (yes/no):");
					String status = s.next();
					if (status.equals("yes") || status.equals("no")) {
						feedback.setResult(status);
						break;
					} else {
						System.out.println("Please Enter only the above domains");
					}
				}
				// This method adds feedback object to list	
				aFeedback.addFeedback(feedback);
				System.out.println("===================================");
				System.out.println("FEEDBACK LIST");
				//Fetching feedback details in fList
				ArrayList<FeedbackPojo> fList = aFeedback.displayFeedback();	
				Iterator<FeedbackPojo> itr = fList.iterator();  		
				while(itr.hasNext()){  			
					FeedbackPojo candidatesList = (FeedbackPojo) itr.next();
				   	System.out.println(candidatesList);  
				}
				navigation();
			break;
			case 3:
				//This method goes back to admin login where HT an TR can access.
				Home.adminLogin();
			break;
		}
	}
	/**
	 * This method is for internal navigation
	 */
	@SuppressWarnings("static-access")
	public void navigation() {	
		Home h = new Home();
		boolean flag = false;
		int choice = 0;
		while(!flag) {
			try {
				System.out.println("");
				System.out.println("===================================");
				System.out.println("press 1 for going back to services");
				System.out.println("press 2 for logout");
				System.out.println("===================================");
				@SuppressWarnings("resource")
				Scanner s=new Scanner(System.in);
				choice = s.nextInt();
				if ((choice == 1)||(choice == 2))
					flag = true;	
			}catch(Exception e) {
				System.out.println("Please enter vaid input");
				flag = false;
			}
		}
		switch(choice){
			case 1:
				//This method is about TR functions
				trFunctions();
				break;
			case 2:	
				//This method goes to main page
				h.login();
				break;
		}
	}
}