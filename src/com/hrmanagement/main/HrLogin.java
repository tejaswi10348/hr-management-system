package com.hrmanagement.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.hrmanagement.bean.AdminPojo;
/**
 * This class contains the HR portal
 * @author Group H
 */
public class HrLogin {
	//This is object of type Array list to store the HR login details.
	static List<AdminPojo> hrList = new ArrayList<AdminPojo>();
	//Admin login details are stored in this file.
	public static final String ADMIN_FILE = "admin.txt";
	static int adminLoginAttempts = 0;
	//The below strings stores the answers for security questions
	static final String MOVIENAME = "kushi";
	static final String SPORT = "kabbadi";
	static final String NATIONALITY = "indian";
	static final String HR_USERNAME = "admin1";
	static final String HR_PASSWORD = "innominds";
	//This method validates the login details of admin
	/**
	 * This method is to validate admin login details.
	 */
	public static void hLogin()  {
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		//This method gets the data from hr list and stores in hrList.
		hrList = getHData();
		HrServices hService = new HrServices();
		System.out.println("Enter Username: ");
		String uName = s.next();
		System.out.println("Enter password: ");
		String uPassword = s.next();
		//Iterator for displaying the data in admin file.
		Iterator<AdminPojo> i = hrList.iterator();
	    	while (i.hasNext()) {
	    		AdminPojo user = (AdminPojo) i.next();
	    		//Checking condition for userId and password
	    		if(user.getUserName().equalsIgnoreCase(uName) && user.getPassword().equalsIgnoreCase(uPassword)) {
	    			System.out.println("");
	    			System.out.println("STATUS:Login Successful");
	    			System.out.println("===================================");
	    			//this method shows the different services HR can perform
	    			hService.hrFunctions();
	    		} else {
	    			System.out.println("wrong credential");
	    			adminLoginAttempts++; 
	    			if(adminLoginAttempts<3) {
	    				System.out.println("you have only "+(3-adminLoginAttempts)+" attempts left");
	    				//This method goes back to HR login portal
	    				hLogin();
	    			} else {
	    				//This method is for checking security questions
	    				hSecurity();	
	    			}
	    		}		
	 		}	
	}
	
	/**
	 * This method checks the security questions.
	 */
	public static void hSecurity() {
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);	
		System.out.println("please choose your security question");
		System.out.println("1.what is your favourite movie");
		System.out.println("2.what is your favourite sport");
		System.out.println("3.what is your nationality");
		int choice = s.nextInt();
		switch(choice) {
		case 1:
			System.out.println("please tyepe the answer");
			String securityAnswer = s.next();
			//validation of security question
			if(securityAnswer.equals(MOVIENAME)) {
				System.out.println("you confirmed your account");	
				System.out.println("your password is " + HR_PASSWORD);
				//This method goes back to HR login portal
				hLogin();
			}else {
				System.out.println("try after sometime");
			}
			break;
		case 2:
			System.out.println("please tyepe the answer");
			String securityAnswer1 = s.next();
			//validation of security question
			if(securityAnswer1.equals(SPORT)) {
				System.out.println("you confirmed your account");		
				System.out.println("your password is " + HR_PASSWORD);
				//This method goes back to HR login portal
				hLogin();
			}else {
				System.out.println("try after sometime");
			}
			break;
	
		case 3:
			System.out.println("please tyepe the answer");
			String securityAnswer2 = s.next();
			//validation of security question
			if(securityAnswer2.equals(NATIONALITY)) {
				System.out.println("you confirmed your account");		
				System.out.println("your password is " + HR_PASSWORD);
				//This method goes back to HR login portal
				hLogin();
			} else {
				System.out.println("try after sometime");
			}
			break;
		}	
	}

	/**
	 * This method sets the HR login details to pojo object.
	 */
	public static void hdata() {
		AdminPojo hr = new AdminPojo();
		hr.setUserName(HR_USERNAME);
		hr.setPassword(HR_PASSWORD);
		hrList.add(hr);
		addHData(hrList);
	}
	
	/**
	 *this methood is to add Admin login details.
	 * @param admin object.
	 */
	public static void addHData(List<AdminPojo> hrList){
		try {
			FileOutputStream file = new FileOutputStream(ADMIN_FILE); 
			ObjectOutputStream out = new ObjectOutputStream(file); 
			//this method is used to write the object to file
			out.writeObject(hrList);
			out.close();
			file.close();
		}  catch (Exception e) {}		
	}
	/**
	 * this method displays the admin login details.
	 * @return admin login details list.
	 */
	@SuppressWarnings("unchecked")
	public static List<AdminPojo> getHData(){
		try {
			FileInputStream file = new FileInputStream(ADMIN_FILE);
			ObjectInputStream in = new ObjectInputStream(file);
			//this method is used to read the object from file
			hrList=(List<AdminPojo>) in.readObject();
			in.close();
			file.close();
		} catch (Exception ex) {} 
	return hrList;
	}
}

