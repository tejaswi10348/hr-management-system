package com.hrmanagement.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.hrmanagement.bean.AdminPojo;
/**
 * This class contains the TR portal
 * @author Group H
 */
public class TrLogin {
	//This is object of type Array list to store the TR login details.
	static List<AdminPojo> trlist = new ArrayList<AdminPojo>();
	//TR user details are stored in this file
	public static final String TR_FILE = "tr.txt";
	static int adminLoginAttempts = 0;
	//the below strings stores the answers for security questions
	static final String MOVIENAME = "kushi";
	static final String SPORT = "kabbadi";
	static final String NATIONALITY = "indian";
	static final String TR_USERNAME = "admin2";
	static final String TR_PASSWORD = "innominds";
	//this method validates the login details of admin
	/**
	 * This method validates the login details of TR. 
	 */
	public static void tLogin()  {  
		@SuppressWarnings("resource")
		Scanner s=new Scanner(System.in);
		trlist = getTData();
		System.out.println("Enter Username: ");
		String uName = s.next();
		System.out.println("Enter password: ");
		String uPassword = s.next();
		//Iterator for getting detailsfrom list.
		Iterator<AdminPojo> i = trlist.iterator();
	    	while (i.hasNext()) {
	    		AdminPojo user = (AdminPojo) i.next();
	    		//Condition for checking login details of TR.
	    		if(user.getUserName().equalsIgnoreCase(uName) && user.getPassword().equalsIgnoreCase(uPassword)) {
	    			System.out.println("");
		    		System.out.println("STATUS:Login Successful");
		    		System.out.println("===================================");
		   			TrServices tr=new TrServices();
		   			//This method is about TR functions.
		   			tr.trFunctions();
		   		} else {
	    			System.out.println("wrong credential"); 
		    		adminLoginAttempts++; 
		   			if(adminLoginAttempts<3) {
		   				System.out.println("you have only "+(3-adminLoginAttempts)+" attempts left");
		   				//This method goes back to TR login portal
		   				tLogin();
	    			} else {
	    				//This method is for checking security questions
	    				tSecurity();	
		    		}
		    	}		
		 	}	
		}
	/**
	 * This method checks the security questions.
	 */
	public static void tSecurity() {
		@SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);
		int trChoice = 0;
		boolean flag = false;
		while(!flag) {
			try {
				System.out.println("please choose your security question");
				System.out.println("1.what is your favourite movie");
				System.out.println("2.what is your favourite sport");
				System.out.println("3.what is your nationality");
				//asking user to choose his security question
				trChoice = s.nextInt();
				if(trChoice == 1 || trChoice == 2 || trChoice == 3) 
					flag = true;
			} catch(Exception e) {
				System.out.println("Please enter vaid input");
				System.out.println("===================================");
				flag = false;
			}
		}
		switch(trChoice) {
			case 1:
				System.out.println("please tyepe the answer");
				String securityAnswer = s.next();
				//validation of security question
				if(securityAnswer.equals(MOVIENAME)) {
					System.out.println("you confirmed your account");	
					System.out.println("your password is " + TR_PASSWORD);	
					//redirecting to admin method for login purpose
					tLogin();
				}else {
					System.out.println("try after sometime");
				}
				break;
				case 2:
					System.out.println("please tyepe the answer");
					String securityAnswer1 = s.next();
					if(securityAnswer1.equals(SPORT)) {
						System.out.println("you confirmed your account");		
						System.out.println("your password is " + TR_PASSWORD);	
						//redirecting to admin method for login purpose
						tLogin();
					}else {
						System.out.println("try after sometime");
					}
					break;
				case 3:
					System.out.println("please tyepe the answer");
					String securityAnswer2 = s.next();
					if(securityAnswer2.equals(NATIONALITY)) {
						System.out.println("you confirmed your account");	
						System.out.println("your password is " + TR_PASSWORD);	
						//redirecting to admin method for login purpose
						tLogin();
					} else {
						System.out.println("try after sometime");
					}
					break;
				}
	}

	/**
	 *this methood is to add TR login details.
	 * @param admin object.
	 */
	public static void tdata() {
		AdminPojo tr = new AdminPojo();
		tr.setUserName(TR_USERNAME);
		tr.setPassword(TR_PASSWORD);
		trlist.add(tr);
		tData(trlist);
	}
	/**
	 *this methood is to add Admin login details.
	 * @param TR object.
	 */
	public static void tData(List<AdminPojo> trlist){
		try {
			FileOutputStream file = new FileOutputStream(TR_FILE); 
			ObjectOutputStream out = new ObjectOutputStream(file); 
			//this method is used to write  the object to file
			out.writeObject(trlist);
			out.close();
			file.close();
		} catch (Exception e) {
		}		
	}
	
	/**
	 * this method displays the TR login details.
	 * @return TR login details list.
	 */
	@SuppressWarnings("unchecked")
	public static List<AdminPojo> getTData(){
		try {
			// Saving of object in a file
			FileInputStream file = new FileInputStream(TR_FILE);
			ObjectInputStream in = new ObjectInputStream(file);
			//this method is used to write  the object to file
			trlist=(List<AdminPojo>) in.readObject();
			in.close();
			file.close();
		} catch (Exception ex) {} 
		return trlist;
	}	
}
